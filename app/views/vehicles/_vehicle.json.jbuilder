json.extract! vehicle, :id, :brand, :model, :power, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)