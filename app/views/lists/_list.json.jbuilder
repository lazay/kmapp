json.extract! list, :id, :date, :reason, :nb_km, :taxe_coef, :ride, :start, :arrival, :created_at, :updated_at
json.url list_url(list, format: :json)