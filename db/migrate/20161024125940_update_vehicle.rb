class UpdateVehicle < ActiveRecord::Migration
  def change
    change_column :vehicles, :vehicle_type, :string
  end
end
