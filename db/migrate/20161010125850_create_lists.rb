class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.datetime :date
      t.string :reason
      t.integer :nb_km
      t.integer :taxe_coef
      t.integer :ride
      t.string :start
      t.string :arrival

      t.timestamps null: false
    end
  end
end
