class AddVehicleIdToList < ActiveRecord::Migration
  def change
    add_column :lists, :vehicle_id, :integer
  end
end
